#!/usr/bin/env python

'''
	Aufa - 11/05/2018
	
	Test PID control for one Joint 

	PID library: http://www.ivmech.com 
'''

import rospy
from ma2000_msg.msg import ma2000Encoder
from ma2000_msg.msg import ma2000Joint
from ma2000_msg.msg import ma2000pwmMotor

import PID

global joint1, joint2, joint3
global encoder1, encoder2, encoder3

# Set default Joint Position
joint1 = 0
joint2 = 0
joint3 = 0
encoder1 = 512
encoder2 = 512
encoder3 = 512

# Set P I D 
Kp_M1 = 10.0
Ki_M1 = 2.0
Kd_M1 = 0.0

pid_M1 = PID.PID(Kp_M1,Ki_M1,Kd_M1)

# Publisher
pubPWM = rospy.Publisher('ma2000pwmMotor', ma2000pwmMotor , queue_size=10)

# From Arduino map function
def map_arduino(x, in_min, in_max, out_min, out_max):
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def pwmMotorPublish(Rpwm1,Lpwm1,Rpwm2,Lpwm2,Rpwm3,Lpwm3):
	data = ma2000pwmMotor()
	data.Rpwm1 = Rpwm1
	data.Lpwm1 = Lpwm1
	data.Rpwm2 = Rpwm2
	data.Lpwm2 = Lpwm2
	data.Rpwm3 = Rpwm3
	data.Lpwm3 = Lpwm3
	pubPWM.publish(data)

def cbEncoder(data):
	global encoder1, encoder2, encoder3
	encoder1 = data.encoderM1
	encoder2 = data.encoderM2
	encoder3 = data.encoderM3

	main_run()

def cbJoint(data):
	global joint1, joint2, joint3
	joint1 = data.joint1
	joint2 = data.joint2
	joint3 = data.joint3

def motor_M1_pid(setpoint1):
	pid_M1.SetPoint = setpoint1
	feedback_M1 = encoder1
	pid_M1.update(feedback_M1)
	output_M1 = pid_M1.output
	
	if output_M1 > 100:
		output_M1 = 100
	elif output_M1 < -100:
		output_M1 = -100

	return output_M1

def main_run():
	global joint1, joint2, joint3
	global encoder1, encoder2, encoder3

	# str_ = ">> {} >> {} >> {}".format(encoder1,encoder2,encoder3)
	# rospy.loginfo(str_)

	setpoint1 = map_arduino(joint1,180,-180,0,1023)	
	
	str_ = "Target > {} : Goal > {}".format(setpoint1,encoder1)
	rospy.loginfo(str_)

	# Besic Control Joint 
	# if(encoder1 > setpoint1):
	# 	pwmMotorPublish(0,50,0,0,0,0)
	# elif(encoder1 < setpoint1):
	# 	pwmMotorPublish(50,0,0,0,0,0)
	# else:
	# 	pwmMotorPublish(0,0,0,0,0,0)

	# PID Control Joint
	output_M1 = motor_M1_pid(setpoint1)
	if output_M1 > 0:
		output_M1 = abs(output_M1)
		pwmMotorPublish(output_M1,0,0,0,0,0)
	elif output_M1 < 0:
		output_M1 = abs(output_M1)
		pwmMotorPublish(0,output_M1,0,0,0,0)
	else:
		pwmMotorPublish(0,0,0,0,0,0)



def ma2000_inverse():

	rospy.init_node('ma2000_PID', anonymous=True)
	
	rospy.Subscriber("ma2000Encoder", ma2000Encoder , cbEncoder)
	rospy.Subscriber("ma2000Joint", ma2000Joint , cbJoint)

	rospy.spin()

if __name__ == '__main__':
	try:
		ma2000_inverse()
	except rospy.ROSInterruptException:
		pass