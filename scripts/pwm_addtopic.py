#!/usr/bin/env python
'''
	Add 3 Topic to 1 Topic PWM
'''
import rospy
from ma2000_msg.msg import ma2000pwmMotor
from std_msgs.msg import Float64

import numpy as np

global data
data = ma2000pwmMotor()
data.Rpwm1 = 0
data.Lpwm1 = 0
data.Rpwm2 = 0
data.Lpwm2 = 0
data.Rpwm3 = 0
data.Lpwm3 = 0
data.Rpwm4 = 0
data.Lpwm4 = 0

# Publisher
pubPWM = rospy.Publisher('ma2000pwmMotor', ma2000pwmMotor , queue_size=10)

def cbPWM_M1(intputM1):
	global data
	output_M1 = np.int(intputM1.data)
	if output_M1 > 0:
		data.Rpwm1 = abs(output_M1)
		data.Lpwm1 = 0
	elif output_M1 < 0:
		data.Rpwm1 = 0
		data.Lpwm1 = abs(output_M1)
	else:
		data.Rpwm1 = 0
		data.Lpwm1 = 0
	

def cbPWM_M2(intputM2):
	global data
	output_M2 = np.int(intputM2.data)
	if output_M2 > 0:
		data.Rpwm2 = abs(output_M2)
		data.Lpwm2 = 0
	elif output_M2 < 0:
		data.Rpwm2 = 0
		data.Lpwm2 = abs(output_M2)
	else:
		data.Rpwm2 = 0
		data.Lpwm2 = 0

def cbPWM_M3(intputM3):
	global data
	output_M3 = np.int(intputM3.data)
	if output_M3 > 0:
		data.Rpwm3 = abs(output_M3)
		data.Lpwm3 = 0
	elif output_M3 < 0:
		data.Rpwm3 = 0
		data.Lpwm3 = abs(output_M3)
	else:
		data.Rpwm3 = 0
		data.Lpwm3 = 0

def cbPWM_M4(intputM4):
	global data
	output_M4 = np.int(intputM4.data)
	if output_M4 > 0:
		data.Rpwm4 = abs(output_M4)
		data.Lpwm4 = 0
	elif output_M4 < 0:
		data.Rpwm4 = 0
		data.Lpwm4 = abs(output_M4)
	else:
		data.Rpwm4 = 0
		data.Lpwm4 = 0
	pubPWM.publish(data)

def pwm_addtopic():

	rospy.init_node('pwm_addtopic', anonymous=True)
	
	rospy.Subscriber("/encoder1/control_effort", Float64 , cbPWM_M1)
	rospy.Subscriber("/encoder2/control_effort", Float64 , cbPWM_M2)
	rospy.Subscriber("/encoder3/control_effort", Float64 , cbPWM_M3)
	rospy.Subscriber("/encoder4/control_effort", Float64 , cbPWM_M4)

	rospy.spin()

if __name__ == '__main__':
	try:
		pwm_addtopic()
	except rospy.ROSInterruptException:
		pass