#!/usr/bin/env python

'''
	Aufa - 11/06/2018

	Inverse kinematic controller code for control MA2000

	PID library: http://www.ivmech.com 
'''

import rospy
from ma2000_msg.msg import ma2000Encoder
from ma2000_msg.msg import ma2000Joint
from ma2000_msg.msg import ma2000pwmMotor
from ma2000_msg.msg import ma2000xyz

import PID
import math as m 

global joint1, joint2, joint3
global encoder1, encoder2, encoder3

# Set default Joint Position
joint1 = 0.0
joint2 = 0.0
joint3 = 0.0
encoder1 = 512
encoder2 = 512
encoder3 = 512

# Set P I D 
Kp_M1 = 10.0
Ki_M1 = 2.0
Kd_M1 = 0.0

Kp_M2 = 10.0
Ki_M2 = 2.0
Kd_M2 = 0.0

Kp_M3 = 10.0
Ki_M3 = 2.0
Kd_M3 = 0.0

pid_M1 = PID.PID(Kp_M1,Ki_M1,Kd_M1)
pid_M2 = PID.PID(Kp_M2,Ki_M2,Kd_M2)
pid_M3 = PID.PID(Kp_M3,Ki_M3,Kd_M3)

# Robot parameters
Pi_const = 3.14159265358979323846
L1 = 230.0
L2 = 250.0
limit_pwm = 255

# Publisher
pubPWM = rospy.Publisher('ma2000pwmMotor', ma2000pwmMotor , queue_size=10)

# From Arduino map function
def map_arduino(x, in_min, in_max, out_min, out_max):
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def pwmMotorPublish(Rpwm1,Lpwm1,Rpwm2,Lpwm2,Rpwm3,Lpwm3):
	data = ma2000pwmMotor()
	data.Rpwm1 = Rpwm1
	data.Lpwm1 = Lpwm1
	data.Rpwm2 = Rpwm2
	data.Lpwm2 = Lpwm2
	data.Rpwm3 = Rpwm3
	data.Lpwm3 = Lpwm3
	pubPWM.publish(data)

def cbEncoder(data):
	global encoder1, encoder2, encoder3
	encoder1 = data.encoderM1
	encoder2 = data.encoderM2
	encoder3 = data.encoderM3

	main_run()

def cbJoint(data):
	global joint1, joint2, joint3
	joint1 = data.joint1
	joint2 = data.joint2
	joint3 = data.joint3

def cbXYZ(data):
	x = data.x
	y = data.y
	z = data.z
	inverse_kinematics(x,y,z)

def inverse_kinematics(x,y,z): # use callback XYZ for Position control
	global joint1, joint2, joint3
	
	theta1 = m.atan2(y,x) #atan2(double y,double x)
	c = m.sqrt(m.pow(x,2)+m.pow(y,2))
	theta3 = m.acos((c*c + z*z - L1*L1 - L2*L2)/(2*L1*L2))
	theta2 = m.atan2(c,z) - m.acos((c*c + z*z + L1*L1 - L2*L2)/(2*L1*m.sqrt(c*c + z*z)))
	joint1 = (180*theta1)/Pi_const
	joint2 = (180*theta2)/Pi_const
	joint3 = (180*theta3)/Pi_const
	# str_ = "Joint 1 : {} ; Joint 2 : {} ; Joint 3 : {}".format(
	# 	joint1_,joint2_,joint3_)
	# rospy.loginfo(str_)


def motor_Mx_pid(setpoint1, setpoint2, setpoint3):
	pid_M1.SetPoint = setpoint1
	feedback_M1 = encoder1
	pid_M1.update(feedback_M1)
	output_M1 = pid_M1.output

	if output_M1 > limit_pwm:
		output_M1 = limit_pwm
	elif output_M1 < -limit_pwm:
		output_M1 = -limit_pwm

	pid_M2.SetPoint = setpoint2
	feedback_M2 = encoder2
	pid_M2.update(feedback_M2)
	output_M2 = pid_M2.output
	
	if output_M2 > limit_pwm:
		output_M2 = limit_pwm
	elif output_M2 < -limit_pwm:
		output_M2 = -limit_pwm

	pid_M3.SetPoint = setpoint3
	feedback_M3 = encoder3
	pid_M3.update(feedback_M3)
	output_M3 = pid_M3.output
	
	if output_M3 > limit_pwm:
		output_M3 = limit_pwm
	elif output_M3 < -limit_pwm:
		output_M3 = -limit_pwm

	return output_M1, output_M2, output_M3

def main_run():
	global joint1, joint2, joint3
	global encoder1, encoder2, encoder3

	setpoint1 = int(map_arduino(joint1,180.0,-180.0,0,1023))
	setpoint2 = int(map_arduino(joint2,-180.0,180.0,0,1023))
	setpoint3 = int(map_arduino(joint3,-180.0,180.0,1023,0))

	# str_ = "Target1: {: 3.1f}, {: 3.1f} > {:3.1f} ; Target2: {: 3.1f}, {: 3.1f} > {:3.1f} ; Target3: {: 3.1f}, {: 3.1f} > {:3.1f} ".format(
	# 	joint1,setpoint1,encoder1,joint2,setpoint2,encoder2,joint3,setpoint3,encoder3)

	del_1 = abs(setpoint1 - encoder1)
	del_2 = abs(setpoint2 - encoder2)
	del_3 = abs(setpoint3 - encoder3)

	str_ = "Target1: {: 3.1f}, {: 3.1f} > {:3.1f} ; Target2: {: 3.1f}, {: 3.1f} > {:3.1f} ; Target3: {: 3.1f}, {: 3.1f} > {:3.1f} ".format(
		setpoint1,encoder1,del_1,setpoint2,encoder2,del_2,setpoint3,encoder3,del_3)
		
	rospy.loginfo(str_)

	# PID Control Joint
	output_M1, output_M2, output_M3 = motor_Mx_pid(setpoint1,setpoint2,setpoint3)
	
	# Direction for drive Robot motors
	if output_M1 > 0:
		Rpwm1 = abs(output_M1)
		Lpwm1 = 0
	elif output_M1 < 0:
		Rpwm1 = 0
		Lpwm1 = abs(output_M1)
	else:
		Rpwm1 = 0
		Lpwm1 = 0
	
	if output_M2 > 0:
		Rpwm2 = abs(output_M2)
		Lpwm2 = 0
	elif output_M2 < 0:
		Rpwm2 = 0
		Lpwm2 = abs(output_M2)
	else:
		Rpwm2 = 0
		Lpwm2 = 0
	
	if output_M3 > 0:
		Rpwm3 = abs(output_M3)
		Lpwm3 = 0
	elif output_M3 < 0:
		Rpwm3 = 0
		Lpwm3 = abs(output_M3)
	else:
		Rpwm3 = 0
		Lpwm3 = 0

	pwmMotorPublish(Rpwm1,Lpwm1,Rpwm2,Lpwm2,Rpwm3,Lpwm3)


def ma2000_inverse():

	rospy.init_node('ma2000_Inverse', anonymous=True)
	
	rospy.Subscriber("ma2000Encoder", ma2000Encoder , cbEncoder)
	rospy.Subscriber("ma2000Joint", ma2000Joint , cbJoint)
	rospy.Subscriber("ma2000xyz", ma2000xyz , cbXYZ)

	rospy.spin()

if __name__ == '__main__':
	try:
		ma2000_inverse()
	except rospy.ROSInterruptException:
		pass