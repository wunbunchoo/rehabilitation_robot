#!/usr/bin/env python

'''
	Aufa - 13/11/2018

	ROS PID controller code for control MA2000 
'''

import rospy
from ma2000_msg.msg import ma2000Joint
from ma2000_msg.msg import ma2000xyz
from ma2000_msg.msg import del_errorX
from std_msgs.msg import Float64

import math as m 
import numpy as np

global joint1, joint2, joint3,joint4
global encoder1, encoder2, encoder3, encoder4

# Set default Joint Position
joint1 = 0.0
joint2 = 0.0
joint3 = 0.0
joint4 = 0.0
encoder1 = 512
encoder2 = 512
encoder3 = 512
encoder4 = 512

# Robot parameters
Pi_const = 3.14159265358979323846
L1 = 230.0
L2 = 250.0

# Publisher
pubSetpoint_M1 = rospy.Publisher('setpoint1', Float64 , queue_size=10)
pubSetpoint_M2 = rospy.Publisher('setpoint2', Float64 , queue_size=10)
pubSetpoint_M3 = rospy.Publisher('setpoint3', Float64 , queue_size=10)
pubSetpoint_M4 = rospy.Publisher('setpoint4', Float64 , queue_size=10)
pubDel_errorx = rospy.Publisher('del_errorX', del_errorX , queue_size=10)

# From Arduino map function
def map_arduino(x, in_min, in_max, out_min, out_max):
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def cbJoint(data):
	global joint1, joint2, joint3, joint4
	joint1 = data.joint1
	joint2 = data.joint2
	joint3 = data.joint3
	joint4 = data.joint4


def cbXYZ(data):
	x = data.x
	y = data.y
	z = data.z
	r = data.r
	inverse_kinematics(x,y,z,r)

def inverse_kinematics(x,y,z,r): # use callback XYZR for Position control
	global joint1, joint2, joint3, joint4
	
	theta1 = m.atan2(y,x) #atan2(double y,double x)
	c = m.sqrt(m.pow(x,2)+m.pow(y,2))
	theta3 = m.acos((c*c + z*z - L1*L1 - L2*L2)/(2*L1*L2))
	theta2 = m.atan2(c,z) - m.acos((c*c + z*z + L1*L1 - L2*L2)/(2*L1*m.sqrt(c*c + z*z)))
	joint1 = (180*theta1)/Pi_const
	joint2 = (180*theta2)/Pi_const
	joint3 = (180*theta3)/Pi_const
	joint4 = 360-joint2-joint3-90

def pub_del_errorX(del_1,del_2,del_3,del_4):
	data = del_errorX()
	data.del_m1 = del_1
	data.del_m2 = del_2
	data.del_m3 = del_3
	data.del_m4 = del_4
	pubDel_errorx.publish(data)

def setpoint_M1_Publish(setpoint):
	data = Float64()
	data.data = np.float64(setpoint)
	pubSetpoint_M1.publish(data)

def setpoint_M2_Publish(setpoint):
	data = Float64()
	data.data = np.float64(setpoint)
	pubSetpoint_M2.publish(data)

def setpoint_M3_Publish(setpoint):
	data = Float64()
	data.data = np.float64(setpoint)
	pubSetpoint_M3.publish(data)

def setpoint_M4_Publish(setpoint):
	data = Float64()
	data.data = np.float64(setpoint)
	pubSetpoint_M4.publish(data)

def cbEncoder1(data):
	global encoder1
	encoder1 = data.data

def cbEncoder2(data):
	global encoder2
	encoder2 = data.data

def cbEncoder3(data):
	global encoder3
	encoder3 = data.data

def cbEncoder4(data):
	global encoder4
	encoder4 = data.data

	main_run()

def main_run():
	global joint1, joint2, joint3,joint4
	global encoder1, encoder2, encoder3, encoder4
	# safety 
	if joint3 <= 150.0 and joint3 >= -150 and joint2 <= 100.0 and joint2 >= -100:
		setpoint1 = int(map_arduino(joint1,180.0,-180.0,0,1023))
		setpoint2 = int(map_arduino(joint2,-180.0,180.0,0,1023))
		setpoint3 = int(map_arduino(joint3,-180.0,180.0,1023,0))
	if joint4 <= 150.0 and joint4 >= -150:
		setpoint4 = int(map_arduino(joint4,-180.0,180.0,1023,0))

	del_1 = abs(setpoint1 - encoder1)
	del_2 = abs(setpoint2 - encoder2)
	del_3 = abs(setpoint3 - encoder3)
	del_4 = abs(setpoint4 - encoder4)
	pub_del_errorX(del_1,del_2,del_3,del_4)
	
	str_ = "Target1:{: 3.1f},{: 3.1f} >{:3.1f} ;Target2:{: 3.1f},{: 3.1f} >{:3.1f} ;Target3:{: 3.1f},{: 3.1f} >{:3.1f};Target4:{: 3.1f},{: 3.1f} >{:3.1f}".format(
		setpoint1,encoder1,del_1,setpoint2,encoder2,del_2,setpoint3,encoder3,del_3,setpoint4,encoder4,del_4)
	rospy.loginfo(str_)

	# PID Control Joint Publish to ROS-PID
	setpoint_M1_Publish(setpoint1)
	setpoint_M2_Publish(setpoint2)
	setpoint_M3_Publish(setpoint3)
	setpoint_M4_Publish(setpoint4)


def ma2000_rospid():

	rospy.init_node('ma2000_rospid', anonymous=True)
	
	rospy.Subscriber("ma2000Joint", ma2000Joint , cbJoint)
	rospy.Subscriber("ma2000xyz", ma2000xyz , cbXYZ)

	rospy.Subscriber("/encoder1/state", Float64, cbEncoder1)
	rospy.Subscriber("/encoder2/state", Float64, cbEncoder2)
	rospy.Subscriber("/encoder3/state", Float64, cbEncoder3)
	rospy.Subscriber("/encoder4/state", Float64, cbEncoder4)

	rospy.spin()

if __name__ == '__main__':
	try:
		ma2000_rospid()
	except rospy.ROSInterruptException:
		pass