#!/usr/bin/env python
'''
	Aufa - 11/07/2018

	Linear parts control for MA2000

	Logic:
		1 loop position N0 to Nx > linear part
		2 inverse get > joint
		3 publish joint 
		4 subscribe encoder > map to joint
		5 check joint > yes do 1
'''

import rospy
from ma2000_msg.msg import ma2000xyz
from ma2000_msg.msg import del_errorX

global del_1,del_2,del_3,del_4
del_1 = 0
del_2 = 0
del_3 = 0
del_4 = 0
offset_m2 = 5 # analog 10bit
offset_m3 = 5
offset_m4 = 5 

# Point setup > Start to Stop
x_set = 250.0
y_set = 0.0

z_start = 350.0 # 230.0
z_stop =  0.0 # 0.0
z_step = 2.0 # step moving point in Millimeters
x_start = 150.0
x_stop = 300.0
x_step = 2.0

global x_move, z_move, direction, z_direction, x_direction
z_move = z_start
x_move = x_start
direction = -1
z_direction = -1
x_direction = 0

pubXYZ = rospy.Publisher('ma2000xyz', ma2000xyz , queue_size=10)

def xyz_pub(px,py,pz):
	data = ma2000xyz()
	data.x = px
	data.y = py 
	data.z = pz 
	pubXYZ.publish(data)

def cbDel_errorX(data):
	global del_1,del_2,del_3,del_3
	del_1 = data.del_m1
	del_2 = data.del_m2
	del_3 = data.del_m3
	del_4 = data.del_m4


	main_run_XZ()

def main_run_Z():
	global del_1,del_2,del_3
	global z_move, direction

	if del_2 < offset_m2 and del_3 < offset_m3:
		if direction == -1:
			z_move = z_move - z_step
		elif direction == 1:
			z_move = z_move + z_step
	if z_move <= z_stop:	
		direction = 1
	elif z_move >= z_start:
		direction = -1
	
	str_ = "Direction {: 1d} : z_move > {: 3.2f} : {: 3.2f}".format(direction,z_move,z_stop)
	rospy.loginfo(str_)

	xyz_pub(x_set,y_set,z_move)	

def main_run_XZ():
	global del_1,del_2,del_3
	global x_move, z_move, z_direction, x_direction

	if del_2 < offset_m2 and del_3 < offset_m3:
		if z_direction == -1 and x_direction == 0:
			z_move = z_move - z_step
		elif z_direction == 1 and x_direction == 0:
			z_move = z_move + z_step
		elif z_direction == 0 and x_direction == 1:
			x_move = x_move + x_step
		elif z_direction == 0 and x_direction == -1:
			x_move = x_move - x_step

	if z_move == z_start and x_move == x_start:
		z_direction = -1
		x_direction = 0
	elif z_move == z_stop and x_move == x_start:	
		z_direction = 0
		x_direction = 1
	elif z_move == z_stop and x_move == x_stop:
		z_direction = 1
		x_direction = 0
	elif z_move == z_start and x_move == x_stop:
		z_direction = 0
		x_direction = -1	

	xyz_pub(x_move,y_set,z_move)

def ma2000_parts():
	rospy.init_node('ma2000_Parts', anonymous=True)

	rospy.Subscriber("del_errorX", del_errorX , cbDel_errorX)

	rospy.spin()

if __name__ == '__main__':
	try:
		ma2000_parts()
	except rospy.ROSInterruptException:
		pass