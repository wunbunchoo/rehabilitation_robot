#!/usr/bin/env python

'''
	Low Pass Filter for Encoder ma2000
'''

import rospy
from ma2000_msg.msg import ma2000Encoder

import numpy as np
from scipy.signal import butter, lfilter, freqz

global encoder1, encoder2, encoder3

# Filter requirements.
order = 6
fs = 100.0       # sample rate, Hz
cutoff = 10  # desired cutoff frequency of the filter, Hz


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def cbEncoder(data):
	global encoder1, encoder2, encoder3
	encoder1 = data.encoderM1
	encoder2 = data.encoderM2
	encoder3 = data.encoderM3

	main_run()

def main_run():
	global encoder1, encoder2, encoder3

	lpf_encoder2 = butter_lowpass_filter(encoder2, cutoff, fs, order)

	str_ = "Encoder2 filter > {:3}".format(lpf_encoder2)
	rospy.loginfo(str_)

def lpf_encoder():
	rospy.init_node('lpf_encoder', anonymous=True)
	
	rospy.Subscriber("ma2000Encoder", ma2000Encoder , cbEncoder)

	rospy.spin()

if __name__ == '__main__':
	try:
		lpf_encoder()
	except rospy.ROSInterruptException:
		pass